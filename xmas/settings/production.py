from .base import *  # noqa


DEBUG = False
TEMPLATES[0]['OPTIONS']['debug'] = False

ALLOWED_HOSTS = ['127.0.0.1', 'localhost', 'kidding.im']

INSTALLED_APPS += [
    'gunicorn',
]

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'