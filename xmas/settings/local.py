from .base import *  # noqa

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = True

# A list of strings designating all applications that are enabled in this Django installation.
# https://docs.djangoproject.com/en/1.11/ref/settings/#installed-apps
INSTALLED_APPS += [
    'debug_toolbar',
]

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

# https://docs.djangoproject.com/en/1.11/ref/settings/#internal-ips
INTERNAL_IPS = ['127.0.0.1', 'localhost', ]