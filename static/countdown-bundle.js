/*
 countdown.js v2.6.0 http://countdownjs.org
 Copyright (c)2006-2014 Stephen M. McKamey.
 Licensed under The MIT License.
*/
var module, countdown = function (v) {
  function A (a, b) {
    var c = a.getTime();
    a.setMonth(a.getMonth() + b);
    return Math.round((a.getTime() - c) / 864E5);
  }

  function w (a) {
    var b = a.getTime(), c = new Date(b);
    c.setMonth(a.getMonth() + 1);
    return Math.round((c.getTime() - b) / 864E5);
  }

  function x (a, b) {
    b = b instanceof Date || null !== b && isFinite(b)
      ? new Date(+b)
      : new Date;
    if (!a) return b;
    var c = +a.value || 0;
    if (c) return b.setTime(b.getTime() + c), b;
    (c = +a.milliseconds || 0) && b.setMilliseconds(b.getMilliseconds() + c);
    (c = +a.seconds || 0) && b.setSeconds(b.getSeconds() +
      c);
    (c = +a.minutes || 0) && b.setMinutes(b.getMinutes() + c);
    (c = +a.hours || 0) && b.setHours(b.getHours() + c);
    (c = +a.weeks || 0) && (c *= 7);
    (c += +a.days || 0) && b.setDate(b.getDate() + c);
    (c = +a.months || 0) && b.setMonth(b.getMonth() + c);
    (c = +a.millennia || 0) && (c *= 10);
    (c += +a.centuries || 0) && (c *= 10);
    (c += +a.decades || 0) && (c *= 10);
    (c += +a.years || 0) && b.setFullYear(b.getFullYear() + c);
    return b;
  }

  function D (a, b) {return y(a) + (1 === a ? p[b] : q[b]);}

  function n () {}

  function k (a, b, c, e, l, d) {
    0 <= a[c] && (b += a[c], delete a[c]);
    b /= l;
    if (1 >= b + 1) return 0;
    if (0 <= a[e]) {
      a[e] =
        +(a[e] + b).toFixed(d);
      switch (e) {
        case 'seconds':
          if (60 !== a.seconds || isNaN(a.minutes)) break;
          a.minutes++;
          a.seconds = 0;
        case 'minutes':
          if (60 !== a.minutes || isNaN(a.hours)) break;
          a.hours++;
          a.minutes = 0;
        case 'hours':
          if (24 !== a.hours || isNaN(a.days)) break;
          a.days++;
          a.hours = 0;
        case 'days':
          if (7 !== a.days || isNaN(a.weeks)) break;
          a.weeks++;
          a.days = 0;
        case 'weeks':
          if (a.weeks !== w(a.refMonth) / 7 || isNaN(a.months)) break;
          a.months++;
          a.weeks = 0;
        case 'months':
          if (12 !== a.months || isNaN(a.years)) break;
          a.years++;
          a.months = 0;
        case 'years':
          if (10 !==
            a.years || isNaN(a.decades)) break;
          a.decades++;
          a.years = 0;
        case 'decades':
          if (10 !== a.decades || isNaN(a.centuries)) break;
          a.centuries++;
          a.decades = 0;
        case 'centuries':
          if (10 !== a.centuries || isNaN(a.millennia)) break;
          a.millennia++;
          a.centuries = 0;
      }
      return 0;
    }
    return b;
  }

  function B (a, b, c, e, l, d) {
    var f = new Date;
    a.start = b = b || f;
    a.end = c = c || f;
    a.units = e;
    a.value = c.getTime() - b.getTime();
    0 > a.value && (f = c, c = b, b = f);
    a.refMonth = new Date(b.getFullYear(), b.getMonth(), 15, 12, 0, 0);
    try {
      a.millennia = 0;
      a.centuries = 0;
      a.decades = 0;
      a.years = c.getFullYear() -
        b.getFullYear();
      a.months = c.getMonth() - b.getMonth();
      a.weeks = 0;
      a.days = c.getDate() - b.getDate();
      a.hours = c.getHours() - b.getHours();
      a.minutes = c.getMinutes() - b.getMinutes();
      a.seconds = c.getSeconds() - b.getSeconds();
      a.milliseconds = c.getMilliseconds() - b.getMilliseconds();
      var g;
      0 > a.milliseconds
        ? (g = s(-a.milliseconds / 1E3), a.seconds -= g, a.milliseconds += 1E3 *
        g)
        : 1E3 <= a.milliseconds &&
        (a.seconds += m(a.milliseconds / 1E3), a.milliseconds %= 1E3);
      0 > a.seconds ? (g = s(-a.seconds / 60), a.minutes -= g, a.seconds += 60 *
        g) : 60 <= a.seconds && (a.minutes +=
        m(a.seconds / 60), a.seconds %= 60);
      0 > a.minutes ? (g = s(-a.minutes / 60), a.hours -= g, a.minutes += 60 *
        g) : 60 <= a.minutes && (a.hours += m(a.minutes / 60), a.minutes %= 60);
      0 > a.hours
        ? (g = s(-a.hours / 24), a.days -= g, a.hours += 24 * g)
        : 24 <= a.hours && (a.days += m(a.hours / 24), a.hours %= 24);
      for (; 0 > a.days;) a.months--, a.days += A(a.refMonth, 1);
      7 <= a.days && (a.weeks += m(a.days / 7), a.days %= 7);
      0 > a.months
        ? (g = s(-a.months / 12), a.years -= g, a.months += 12 * g)
        : 12 <= a.months && (a.years += m(a.months / 12), a.months %= 12);
      10 <= a.years && (a.decades += m(a.years / 10), a.years %=
        10, 10 <= a.decades &&
      (a.centuries += m(a.decades / 10), a.decades %= 10, 10 <= a.centuries &&
      (a.millennia += m(a.centuries / 10), a.centuries %= 10)));
      b = 0;
      !(e & 1024) || b >= l ? (a.centuries += 10 *
        a.millennia, delete a.millennia) : a.millennia && b++;
      !(e & 512) || b >= l
        ? (a.decades += 10 * a.centuries, delete a.centuries)
        : a.centuries && b++;
      !(e & 256) || b >= l
        ? (a.years += 10 * a.decades, delete a.decades)
        : a.decades && b++;
      !(e & 128) || b >= l
        ? (a.months += 12 * a.years, delete a.years)
        : a.years && b++;
      !(e & 64) || b >= l ? (a.months &&
      (a.days += A(a.refMonth, a.months)), delete a.months,
      7 <= a.days && (a.weeks += m(a.days / 7), a.days %= 7)) : a.months && b++;
      !(e & 32) || b >= l ? (a.days += 7 * a.weeks, delete a.weeks) : a.weeks &&
        b++;
      !(e & 16) || b >= l ? (a.hours += 24 * a.days, delete a.days) : a.days &&
        b++;
      !(e & 8) || b >= l
        ? (a.minutes += 60 * a.hours, delete a.hours)
        : a.hours && b++;
      !(e & 4) || b >= l
        ? (a.seconds += 60 * a.minutes, delete a.minutes)
        : a.minutes && b++;
      !(e & 2) || b >= l
        ? (a.milliseconds += 1E3 * a.seconds, delete a.seconds)
        : a.seconds && b++;
      if (!(e & 1) || b >= l) {
        var h = k(a, 0, 'milliseconds', 'seconds', 1E3, d);
        if (h && (h = k(a, h, 'seconds', 'minutes', 60, d)) && (h =
            k(a, h, 'minutes', 'hours', 60, d)) &&
          (h = k(a, h, 'hours', 'days', 24, d)) &&
          (h = k(a, h, 'days', 'weeks', 7, d)) &&
          (h = k(a, h, 'weeks', 'months', w(a.refMonth) / 7, d))) {
          e = h;
          var n, p = a.refMonth, q = p.getTime(), r = new Date(q);
          r.setFullYear(p.getFullYear() + 1);
          n = Math.round((r.getTime() - q) / 864E5);
          if (h = k(a, e, 'months', 'years', n / w(a.refMonth), d)) if (h = k(a,
            h, 'years', 'decades', 10, d)) if (h = k(a, h, 'decades',
            'centuries', 10, d)) if (h = k(a, h, 'centuries', 'millennia', 10,
            d)) throw Error('Fractional unit overflow');
        }
      }
    } finally {delete a.refMonth;}
    return a;
  }

  function d (a, b, c, e, d) {
    var f;
    c = +c || 222;
    e = 0 < e ? e : NaN;
    d = 0 < d ? 20 > d ? Math.round(d) : 20 : 0;
    var k = null;
    'function' === typeof a ? (f = a, a = null) : a instanceof Date ||
      (null !== a && isFinite(a) ? a = new Date(+a) : ('object' === typeof k &&
      (k = a), a = null));
    var g = null;
    'function' === typeof b ? (f = b, b = null) : b instanceof Date ||
      (null !== b && isFinite(b) ? b = new Date(+b) : ('object' === typeof b &&
      (g = b), b = null));
    k && (a = x(k, b));
    g && (b = x(g, a));
    if (!a && !b) return new n;
    if (!f) return B(new n, a, b, c, e, d);
    var k = c & 1 ? 1E3 / 30 : c & 2 ? 1E3 : c & 4 ? 6E4 : c & 8 ? 36E5 : c & 16
      ? 864E5
      : 6048E5,
      h, g = function () {f(B(new n, a, b, c, e, d), h);};
    g();
    return h = setInterval(g, k);
  }

  var s = Math.ceil, m = Math.floor, p, q, r, t, u, f, y, z;
  n.prototype.toString = function (a) {
    var b = z(this), c = b.length;
    if (!c) return a ? '' + a : u;
    if (1 === c) return b[0];
    a = r + b.pop();
    return b.join(t) + a;
  };
  n.prototype.toHTML = function (a, b) {
    a = a || 'span';
    var c = z(this), e = c.length;
    if (!e) return (b = b || u)
      ? '\x3c' + a + '\x3e' + b + '\x3c/' + a + '\x3e'
      : b;
    for (var d = 0; d < e; d++) c[d] = '\x3c' + a + '\x3e' + c[d] + '\x3c/' +
      a + '\x3e';
    if (1 === e) return c[0];
    e = r + c.pop();
    return c.join(t) + e;
  };
  n.prototype.addTo =
    function (a) {return x(this, a);};
  z = function (a) {
    var b = [], c = a.millennia;
    c && b.push(f(c, 10));
    (c = a.centuries) && b.push(f(c, 9));
    (c = a.decades) && b.push(f(c, 8));
    (c = a.years) && b.push(f(c, 7));
    (c = a.months) && b.push(f(c, 6));
    (c = a.weeks) && b.push(f(c, 5));
    (c = a.days) && b.push(f(c, 4));
    (c = a.hours) && b.push(f(c, 3));
    (c = a.minutes) && b.push(f(c, 2));
    (c = a.seconds) && b.push(f(c, 1));
    (c = a.milliseconds) && b.push(f(c, 0));
    return b;
  };
  d.MILLISECONDS = 1;
  d.SECONDS = 2;
  d.MINUTES = 4;
  d.HOURS = 8;
  d.DAYS = 16;
  d.WEEKS = 32;
  d.MONTHS = 64;
  d.YEARS = 128;
  d.DECADES = 256;
  d.CENTURIES = 512;
  d.MILLENNIA = 1024;
  d.DEFAULTS = 222;
  d.ALL = 2047;
  var E = d.setFormat = function (a) {
    if (a) {
      if ('singular' in a || 'plural' in a) {
        var b = a.singular || [];
        b.split && (b = b.split('|'));
        var c = a.plural || [];
        c.split && (c = c.split('|'));
        for (var d = 0; 10 >= d; d++) p[d] = b[d] || p[d], q[d] = c[d] || q[d];
      }
      'string' === typeof a.last && (r = a.last);
      'string' === typeof a.delim && (t = a.delim);
      'string' === typeof a.empty && (u = a.empty);
      'function' === typeof a.formatNumber && (y = a.formatNumber);
      'function' === typeof a.formatter && (f = a.formatter);
    }
  }, C = d.resetFormat =
    function () {
      p = ' millisecond; second; minute; hour; day; week; month; year; decade; century; millennium'.split(
        ';');
      q = ' milliseconds; seconds; minutes; hours; days; weeks; months; years; decades; centuries; millennia'.split(
        ';');
      r = ' and ';
      t = ', ';
      u = '';
      y = function (a) {return a;};
      f = D;
    };
  d.setLabels = function (a, b, c, d, f, k, m) {
    E({
      singular: a,
      plural: b,
      last: c,
      delim: d,
      empty: f,
      formatNumber: k,
      formatter: m,
    });
  };
  d.resetLabels = C;
  C();
  v && v.exports ? v.exports = d : 'function' === typeof window.define &&
    'undefined' !== typeof window.define.amd &&
    window.define('countdown', [], function () {return d;});
  return d;
}(module);

/*! howler.js v2.0.15 | (c) 2013-2018, James Simpson of GoldFire Studios | MIT License | howlerjs.com */
!function () {
  'use strict';
  var e = function () {this.init();};
  e.prototype = {
    init: function () {
      var e = this || n;
      return e._counter = 1e3, e._codecs = {}, e._howls = [], e._muted = !1, e._volume = 1, e._canPlayEvent = 'canplaythrough', e._navigator = 'undefined' !=
      typeof window && window.navigator
        ? window.navigator
        : null, e.masterGain = null, e.noAudio = !1, e.usingWebAudio = !0, e.autoSuspend = !0, e.ctx = null, e.mobileAutoEnable = !0, e._setup(), e;
    },
    volume: function (e) {
      var o = this || n;
      if (e = parseFloat(e), o.ctx || _(), void 0 !== e && e >= 0 && e <= 1) {
        if (o._volume = e, o._muted) return o;
        o.usingWebAudio &&
        o.masterGain.gain.setValueAtTime(e, n.ctx.currentTime);
        for (var t = 0; t <
        o._howls.length; t++) if (!o._howls[t]._webAudio) for (var r = o._howls[t]._getSoundIds(), a = 0; a <
        r.length; a++) {
          var u = o._howls[t]._soundById(r[a]);
          u && u._node && (u._node.volume = u._volume * e);
        }
        return o;
      }
      return o._volume;
    },
    mute: function (e) {
      var o = this || n;
      o.ctx || _(), o._muted = e, o.usingWebAudio &&
      o.masterGain.gain.setValueAtTime(e ? 0 : o._volume, n.ctx.currentTime);
      for (var t = 0; t <
      o._howls.length; t++) if (!o._howls[t]._webAudio) for (var r = o._howls[t]._getSoundIds(), a = 0; a <
      r.length; a++) {
        var u = o._howls[t]._soundById(r[a]);
        u && u._node && (u._node.muted = !!e || u._muted);
      }
      return o;
    },
    unload: function () {
      for (var e = this || n, o = e._howls.length - 1; o >=
      0; o--) e._howls[o].unload();
      return e.usingWebAudio && e.ctx && void 0 !== e.ctx.close &&
      (e.ctx.close(), e.ctx = null, _()), e;
    },
    codecs: function (e) {return (this || n)._codecs[e.replace(/^x-/, '')];},
    _setup: function () {
      var e = this || n;
      if (e.state = e.ctx
        ? e.ctx.state || 'running'
        : 'running', e._autoSuspend(), !e.usingWebAudio) if ('undefined' !=
        typeof Audio) try {
        var o = new Audio;
        void 0 === o.oncanplaythrough && (e._canPlayEvent = 'canplay');
      } catch (n) {e.noAudio = !0;} else e.noAudio = !0;
      try {
        var o = new Audio;
        o.muted && (e.noAudio = !0);
      } catch (e) {}
      return e.noAudio || e._setupCodecs(), e;
    },
    _setupCodecs: function () {
      var e = this || n, o = null;
      try {
        o = 'undefined' != typeof Audio
          ? new Audio
          : null;
      } catch (n) {return e;}
      if (!o || 'function' != typeof o.canPlayType) return e;
      var t = o.canPlayType('audio/mpeg;').replace(/^no$/, ''),
        r = e._navigator && e._navigator.userAgent.match(/OPR\/([0-6].)/g),
        a = r && parseInt(r[0].split('/')[1], 10) < 33;
      return e._codecs = {
        mp3: !(a || !t && !o.canPlayType('audio/mp3;').replace(/^no$/, '')),
        mpeg: !!t,
        opus: !!o.canPlayType('audio/ogg; codecs="opus"').replace(/^no$/, ''),
        ogg: !!o.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ''),
        oga: !!o.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ''),
        wav: !!o.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ''),
        aac: !!o.canPlayType('audio/aac;').replace(/^no$/, ''),
        caf: !!o.canPlayType('audio/x-caf;').replace(/^no$/, ''),
        m4a: !!(o.canPlayType('audio/x-m4a;') || o.canPlayType('audio/m4a;') ||
          o.canPlayType('audio/aac;')).replace(/^no$/, ''),
        mp4: !!(o.canPlayType('audio/x-mp4;') || o.canPlayType('audio/mp4;') ||
          o.canPlayType('audio/aac;')).replace(/^no$/, ''),
        weba: !!o.canPlayType('audio/webm; codecs="vorbis"').
          replace(/^no$/, ''),
        webm: !!o.canPlayType('audio/webm; codecs="vorbis"').
          replace(/^no$/, ''),
        dolby: !!o.canPlayType('audio/mp4; codecs="ec-3"').replace(/^no$/, ''),
        flac: !!(o.canPlayType('audio/x-flac;') ||
          o.canPlayType('audio/flac;')).replace(/^no$/, ''),
      }, e;
    },
    _enableMobileAudio: function () {
      var e = this || n,
        o = /iPhone|iPad|iPod|Android|BlackBerry|BB10|Silk|Mobi|Chrome/i.test(
          e._navigator && e._navigator.userAgent);
      if (!e._mobileEnabled && e.ctx && o) {
        e._mobileEnabled = !1, e.mobileAutoEnable = !1, e._mobileUnloaded ||
        44100 === e.ctx.sampleRate ||
        (e._mobileUnloaded = !0, e.unload()), e._scratchBuffer = e.ctx.createBuffer(
          1, 1, 22050);
        var t = function (o) {
          n._autoResume();
          var r = e.ctx.createBufferSource();
          r.buffer = e._scratchBuffer, r.connect(e.ctx.destination), void 0 ===
          r.start ? r.noteOn(0) : r.start(0), 'function' ==
          typeof e.ctx.resume && e.ctx.resume(), r.onended = function () {
            r.disconnect(
              0), e._mobileEnabled = !0, document.removeEventListener(
              'touchstart', t, !0), document.removeEventListener('touchend', t,
              !0), document.removeEventListener('click', t, !0);
            for (var n = 0; n < e._howls.length; n++) e._howls[n]._emit(
              'unlock');
          };
        };
        return document.addEventListener('touchstart', t,
          !0), document.addEventListener('touchend', t,
          !0), document.addEventListener('click', t, !0), e;
      }
    },
    _autoSuspend: function () {
      var e = this;
      if (e.autoSuspend && e.ctx && void 0 !== e.ctx.suspend &&
        n.usingWebAudio) {
        for (var o = 0; o <
        e._howls.length; o++) if (e._howls[o]._webAudio) for (var t = 0; t <
        e._howls[o]._sounds.length; t++) if (!e._howls[o]._sounds[t]._paused) return e;
        return e._suspendTimer &&
        clearTimeout(e._suspendTimer), e._suspendTimer = setTimeout(
          function () {
            e.autoSuspend &&
            (e._suspendTimer = null, e.state = 'suspending', e.ctx.suspend().
              then(function () {
                e.state = 'suspended', e._resumeAfterSuspend &&
                (delete e._resumeAfterSuspend, e._autoResume());
              }));
          }, 3e4), e;
      }
    },
    _autoResume: function () {
      var e = this;
      if (e.ctx && void 0 !== e.ctx.resume &&
        n.usingWebAudio) return 'running' === e.state && e._suspendTimer
        ? (clearTimeout(e._suspendTimer), e._suspendTimer = null)
        : 'suspended' === e.state
          ? (e.ctx.resume().
            then(function () {
              e.state = 'running';
              for (var n = 0; n < e._howls.length; n++) e._howls[n]._emit(
                'resume');
            }), e._suspendTimer &&
          (clearTimeout(e._suspendTimer), e._suspendTimer = null))
          : 'suspending' === e.state && (e._resumeAfterSuspend = !0), e;
    },
  };
  var n = new e, o = function (e) {
    var n = this;
    if (!e.src || 0 === e.src.length) return void console.error(
      'An array of source files must be passed with any new Howl.');
    n.init(e);
  };
  o.prototype = {
    init: function (e) {
      var o = this;
      return n.ctx || _(), o._autoplay = e.autoplay ||
        !1, o._format = 'string' != typeof e.format
        ? e.format
        : [e.format], o._html5 = e.html5 || !1, o._muted = e.mute ||
        !1, o._loop = e.loop || !1, o._pool = e.pool ||
        5, o._preload = 'boolean' != typeof e.preload ||
        e.preload, o._rate = e.rate || 1, o._sprite = e.sprite ||
        {}, o._src = 'string' != typeof e.src
        ? e.src
        : [e.src], o._volume = void 0 !== e.volume
        ? e.volume
        : 1, o._xhrWithCredentials = e.xhrWithCredentials ||
        !1, o._duration = 0, o._state = 'unloaded', o._sounds = [], o._endTimers = {}, o._queue = [], o._playLock = !1, o._onend = e.onend
        ? [{ fn: e.onend }]
        : [], o._onfade = e.onfade
        ? [{ fn: e.onfade }]
        : [], o._onload = e.onload
        ? [{ fn: e.onload }]
        : [], o._onloaderror = e.onloaderror
        ? [{ fn: e.onloaderror }]
        : [], o._onplayerror = e.onplayerror
        ? [{ fn: e.onplayerror }]
        : [], o._onpause = e.onpause
        ? [{ fn: e.onpause }]
        : [], o._onplay = e.onplay
        ? [{ fn: e.onplay }]
        : [], o._onstop = e.onstop
        ? [{ fn: e.onstop }]
        : [], o._onmute = e.onmute
        ? [{ fn: e.onmute }]
        : [], o._onvolume = e.onvolume
        ? [{ fn: e.onvolume }]
        : [], o._onrate = e.onrate
        ? [{ fn: e.onrate }]
        : [], o._onseek = e.onseek
        ? [{ fn: e.onseek }]
        : [], o._onunlock = e.onunlock
        ? [{ fn: e.onunlock }]
        : [], o._onresume = [], o._webAudio = n.usingWebAudio &&
        !o._html5, void 0 !== n.ctx && n.ctx && n.mobileAutoEnable &&
      n._enableMobileAudio(), n._howls.push(o), o._autoplay && o._queue.push(
        { event: 'play', action: function () {o.play();} }), o._preload &&
      o.load(), o;
    },
    load: function () {
      var e = this, o = null;
      if (n.noAudio) return void e._emit('loaderror', null,
        'No audio support.');
      'string' == typeof e._src && (e._src = [e._src]);
      for (var r = 0; r < e._src.length; r++) {
        var u, i;
        if (e._format && e._format[r]) u = e._format[r]; else {
          if ('string' != typeof (i = e._src[r])) {
            e._emit('loaderror', null,
              'Non-string found in selected audio sources - ignoring.');
            continue;
          }
          u = /^data:audio\/([^;,]+);/i.exec(i), u ||
          (u = /\.([^.]+)$/.exec(i.split('?', 1)[0])), u &&
          (u = u[1].toLowerCase());
        }
        if (u || console.warn(
          'No file extension was found. Consider using the "format" property or specify an extension.'), u &&
        n.codecs(u)) {
          o = e._src[r];
          break;
        }
      }
      return o
        ? (e._src = o, e._state = 'loading', 'https:' ===
        window.location.protocol && 'http:' === o.slice(0, 5) &&
        (e._html5 = !0, e._webAudio = !1), new t(e), e._webAudio && a(e), e)
        : void e._emit('loaderror', null,
          'No codec support for selected audio sources.');
    },
    play: function (e, o) {
      var t = this, r = null;
      if ('number' == typeof e) r = e, e = null; else {
        if ('string' == typeof e && 'loaded' === t._state &&
          !t._sprite[e]) return null;
        if (void 0 === e) {
          e = '__default';
          for (var a = 0, u = 0; u <
          t._sounds.length; u++) t._sounds[u]._paused && !t._sounds[u]._ended &&
          (a++, r = t._sounds[u]._id);
          1 === a ? e = null : r = null;
        }
      }
      var i = r ? t._soundById(r) : t._inactiveSound();
      if (!i) return null;
      if (r && !e && (e = i._sprite || '__default'), 'loaded' !== t._state) {
        i._sprite = e, i._ended = !1;
        var d = i._id;
        return t._queue.push(
          { event: 'play', action: function () {t.play(d);} }), d;
      }
      if (r && !i._paused) return o || t._loadQueue('play'), i._id;
      t._webAudio && n._autoResume();
      var _ = Math.max(0, i._seek > 0 ? i._seek : t._sprite[e][0] / 1e3),
        s = Math.max(0, (t._sprite[e][0] + t._sprite[e][1]) / 1e3 - _),
        l = 1e3 * s / Math.abs(i._rate);
      if (i._paused = !1, i._ended = !1, i._sprite = e, i._seek = _, i._start = t._sprite[e][0] /
        1e3, i._stop = (t._sprite[e][0] + t._sprite[e][1]) /
        1e3, i._loop = !(!i._loop && !t._sprite[e][2]), i._seek >=
      i._stop) return void t._ended(i);
      var c = i._node;
      if (t._webAudio) {
        var f = function () {
          t._refreshBuffer(i);
          var e = i._muted || t._muted ? 0 : i._volume;
          c.gain.setValueAtTime(e,
            n.ctx.currentTime), i._playStart = n.ctx.currentTime, void 0 ===
          c.bufferSource.start ? i._loop
            ? c.bufferSource.noteGrainOn(0, _, 86400)
            : c.bufferSource.noteGrainOn(0, _, s) : i._loop
            ? c.bufferSource.start(0, _, 86400)
            : c.bufferSource.start(0, _, s), l !== 1 / 0 &&
          (t._endTimers[i._id] = setTimeout(t._ended.bind(t, i), l)), o ||
          setTimeout(function () {t._emit('play', i._id);}, 0);
        };
        'running' === n.state ? f() : (t.once('resume', f), t._clearTimer(
          i._id));
      } else {
        var p = function () {
          c.currentTime = _, c.muted = i._muted || t._muted || n._muted ||
            c.muted, c.volume = i._volume *
            n.volume(), c.playbackRate = i._rate;
          try {
            var r = c.play();
            if (r && 'undefined' != typeof Promise &&
            (r instanceof Promise || 'function' == typeof r.then)
              ? (t._playLock = !0, r.then(
                function () {t._playLock = !1, o || t._emit('play', i._id);}).
                catch(function () {
                  t._playLock = !1, t._emit('playerror', i._id,
                    'Playback was unable to start. This is most commonly an issue on mobile devices and Chrome where playback was not within a user interaction.');
                }))
              : o || t._emit('play',
              i._id), c.playbackRate = i._rate, c.paused) return void t._emit(
              'playerror', i._id,
              'Playback was unable to start. This is most commonly an issue on mobile devices and Chrome where playback was not within a user interaction.');
            '__default' !== e || i._loop
              ? t._endTimers[i._id] = setTimeout(t._ended.bind(t, i), l)
              : (t._endTimers[i._id] = function () {
                t._ended(i), c.removeEventListener('ended', t._endTimers[i._id],
                  !1);
              }, c.addEventListener('ended', t._endTimers[i._id], !1));
          } catch (e) {t._emit('playerror', i._id, e);}
        }, m = window && window.ejecta || !c.readyState &&
          n._navigator.isCocoonJS;
        if (c.readyState >= 3 || m) p(); else {
          var v = function () {
            p(), c.removeEventListener(n._canPlayEvent, v, !1);
          };
          c.addEventListener(n._canPlayEvent, v, !1), t._clearTimer(i._id);
        }
      }
      return i._id;
    },
    pause: function (e) {
      var n = this;
      if ('loaded' !== n._state || n._playLock) return n._queue.push(
        { event: 'pause', action: function () {n.pause(e);} }), n;
      for (var o = n._getSoundIds(e), t = 0; t < o.length; t++) {
        n._clearTimer(o[t]);
        var r = n._soundById(o[t]);
        if (r && !r._paused &&
          (r._seek = n.seek(o[t]), r._rateSeek = 0, r._paused = !0, n._stopFade(
            o[t]), r._node)) if (n._webAudio) {
          if (!r._node.bufferSource) continue;
          void 0 === r._node.bufferSource.stop
            ? r._node.bufferSource.noteOff(0)
            : r._node.bufferSource.stop(0), n._cleanBuffer(r._node);
        } else isNaN(r._node.duration) && r._node.duration !== 1 / 0 ||
        r._node.pause();
        arguments[1] || n._emit('pause', r ? r._id : null);
      }
      return n;
    },
    stop: function (e, n) {
      var o = this;
      if ('loaded' !== o._state || o._playLock) return o._queue.push(
        { event: 'stop', action: function () {o.stop(e);} }), o;
      for (var t = o._getSoundIds(e), r = 0; r < t.length; r++) {
        o._clearTimer(t[r]);
        var a = o._soundById(t[r]);
        a && (a._seek = a._start ||
          0, a._rateSeek = 0, a._paused = !0, a._ended = !0, o._stopFade(
          t[r]), a._node && (o._webAudio ? a._node.bufferSource &&
          (void 0 === a._node.bufferSource.stop ? a._node.bufferSource.noteOff(
            0) : a._node.bufferSource.stop(0), o._cleanBuffer(a._node)) : isNaN(
          a._node.duration) && a._node.duration !== 1 / 0 ||
          (a._node.currentTime = a._start || 0, a._node.pause())), n ||
        o._emit('stop', a._id));
      }
      return o;
    },
    mute: function (e, o) {
      var t = this;
      if ('loaded' !== t._state || t._playLock) return t._queue.push(
        { event: 'mute', action: function () {t.mute(e, o);} }), t;
      if (void 0 === o) {
        if ('boolean' != typeof e) return t._muted;
        t._muted = e;
      }
      for (var r = t._getSoundIds(o), a = 0; a < r.length; a++) {
        var u = t._soundById(r[a]);
        u &&
        (u._muted = e, u._interval && t._stopFade(u._id), t._webAudio && u._node
          ? u._node.gain.setValueAtTime(e ? 0 : u._volume, n.ctx.currentTime)
          : u._node && (u._node.muted = !!n._muted || e), t._emit('mute',
          u._id));
      }
      return t;
    },
    volume: function () {
      var e, o, t = this, r = arguments;
      if (0 === r.length) return t._volume;
      if (1 === r.length || 2 === r.length && void 0 === r[1]) {
        t._getSoundIds().indexOf(r[0]) >= 0
          ? o = parseInt(r[0], 10)
          : e = parseFloat(r[0]);
      } else r.length >= 2 && (e = parseFloat(r[0]), o = parseInt(r[1], 10));
      var a;
      if (!(void 0 !== e && e >= 0 && e <= 1)) return a = o
        ? t._soundById(o)
        : t._sounds[0], a ? a._volume : 0;
      if ('loaded' !== t._state || t._playLock) return t._queue.push(
        { event: 'volume', action: function () {t.volume.apply(t, r);} }), t;
      void 0 === o && (t._volume = e), o = t._getSoundIds(o);
      for (var u = 0; u < o.length; u++) (a = t._soundById(o[u])) &&
      (a._volume = e, r[2] || t._stopFade(o[u]), t._webAudio && a._node &&
      !a._muted ? a._node.gain.setValueAtTime(e, n.ctx.currentTime) : a._node &&
        !a._muted && (a._node.volume = e * n.volume()), t._emit('volume',
        a._id));
      return t;
    },
    fade: function (e, o, t, r) {
      var a = this;
      if ('loaded' !== a._state || a._playLock) return a._queue.push(
        { event: 'fade', action: function () {a.fade(e, o, t, r);} }), a;
      a.volume(e, r);
      for (var u = a._getSoundIds(r), i = 0; i < u.length; i++) {
        var d = a._soundById(u[i]);
        if (d) {
          if (r || a._stopFade(u[i]), a._webAudio && !d._muted) {
            var _ = n.ctx.currentTime, s = _ + t / 1e3;
            d._volume = e, d._node.gain.setValueAtTime(e,
              _), d._node.gain.linearRampToValueAtTime(o, s);
          }
          a._startFadeInterval(d, e, o, t, u[i], void 0 === r);
        }
      }
      return a;
    },
    _startFadeInterval: function (e, n, o, t, r, a) {
      var u = this, i = n, d = o - n, _ = Math.abs(d / .01),
        s = Math.max(4, _ > 0 ? t / _ : t), l = Date.now();
      e._fadeTo = o, e._interval = setInterval(function () {
        var r = (Date.now() - l) / t;
        l = Date.now(), i += d * r, i = Math.max(0, i), i = Math.min(1,
          i), i = Math.round(100 * i) / 100, u._webAudio
          ? e._volume = i
          : u.volume(i, e._id, !0), a && (u._volume = i), (o < n && i <= o ||
          o > n && i >= o) && (clearInterval(
          e._interval), e._interval = null, e._fadeTo = null, u.volume(o,
          e._id), u._emit('fade', e._id));
      }, s);
    },
    _stopFade: function (e) {
      var o = this, t = o._soundById(e);
      return t && t._interval && (o._webAudio &&
      t._node.gain.cancelScheduledValues(n.ctx.currentTime), clearInterval(
        t._interval), t._interval = null, o.volume(t._fadeTo,
        e), t._fadeTo = null, o._emit('fade', e)), o;
    },
    loop: function () {
      var e, n, o, t = this, r = arguments;
      if (0 === r.length) return t._loop;
      if (1 === r.length) {
        if ('boolean' != typeof r[0]) return !!(o = t._soundById(
          parseInt(r[0], 10))) && o._loop;
        e = r[0], t._loop = e;
      } else 2 === r.length && (e = r[0], n = parseInt(r[1], 10));
      for (var a = t._getSoundIds(n), u = 0; u <
      a.length; u++) (o = t._soundById(a[u])) &&
      (o._loop = e, t._webAudio && o._node && o._node.bufferSource &&
      (o._node.bufferSource.loop = e, e &&
      (o._node.bufferSource.loopStart = o._start ||
        0, o._node.bufferSource.loopEnd = o._stop)));
      return t;
    },
    rate: function () {
      var e, o, t = this, r = arguments;
      if (0 === r.length) o = t._sounds[0]._id; else if (1 === r.length) {
        var a = t._getSoundIds(), u = a.indexOf(r[0]);
        u >= 0 ? o = parseInt(r[0], 10) : e = parseFloat(r[0]);
      } else 2 === r.length && (e = parseFloat(r[0]), o = parseInt(r[1], 10));
      var i;
      if ('number' != typeof e) return i = t._soundById(o), i
        ? i._rate
        : t._rate;
      if ('loaded' !== t._state || t._playLock) return t._queue.push(
        { event: 'rate', action: function () {t.rate.apply(t, r);} }), t;
      void 0 === o && (t._rate = e), o = t._getSoundIds(o);
      for (var d = 0; d < o.length; d++) if (i = t._soundById(o[d])) {
        i._rateSeek = t.seek(o[d]), i._playStart = t._webAudio
          ? n.ctx.currentTime
          : i._playStart, i._rate = e, t._webAudio && i._node &&
        i._node.bufferSource ? i._node.bufferSource.playbackRate.setValueAtTime(
          e, n.ctx.currentTime) : i._node && (i._node.playbackRate = e);
        var _ = t.seek(o[d]),
          s = (t._sprite[i._sprite][0] + t._sprite[i._sprite][1]) / 1e3 - _,
          l = 1e3 * s / Math.abs(i._rate);
        !t._endTimers[o[d]] && i._paused ||
        (t._clearTimer(o[d]), t._endTimers[o[d]] = setTimeout(
          t._ended.bind(t, i), l)), t._emit('rate', i._id);
      }
      return t;
    },
    seek: function () {
      var e, o, t = this, r = arguments;
      if (0 === r.length) o = t._sounds[0]._id; else if (1 === r.length) {
        var a = t._getSoundIds(), u = a.indexOf(r[0]);
        u >= 0 ? o = parseInt(r[0], 10) : t._sounds.length &&
          (o = t._sounds[0]._id, e = parseFloat(r[0]));
      } else 2 === r.length && (e = parseFloat(r[0]), o = parseInt(r[1], 10));
      if (void 0 === o) return t;
      if ('loaded' !== t._state || t._playLock) return t._queue.push(
        { event: 'seek', action: function () {t.seek.apply(t, r);} }), t;
      var i = t._soundById(o);
      if (i) {
        if (!('number' == typeof e && e >= 0)) {
          if (t._webAudio) {
            var d = t.playing(o) ? n.ctx.currentTime - i._playStart : 0,
              _ = i._rateSeek ? i._rateSeek - i._seek : 0;
            return i._seek + (_ + d * Math.abs(i._rate));
          }
          return i._node.currentTime;
        }
        var s = t.playing(o);
        s && t.pause(o, !0), i._seek = e, i._ended = !1, t._clearTimer(
          o), !t._webAudio && i._node && (i._node.currentTime = e);
        var l = function () {t._emit('seek', o), s && t.play(o, !0);};
        if (s && !t._webAudio) {
          var c = function () {
            t._playLock ? setTimeout(c, 0) : l();
          };
          setTimeout(c, 0);
        } else l();
      }
      return t;
    },
    playing: function (e) {
      var n = this;
      if ('number' == typeof e) {
        var o = n._soundById(e);
        return !!o && !o._paused;
      }
      for (var t = 0; t <
      n._sounds.length; t++) if (!n._sounds[t]._paused) return !0;
      return !1;
    },
    duration: function (e) {
      var n = this, o = n._duration, t = n._soundById(e);
      return t && (o = n._sprite[t._sprite][1] / 1e3), o;
    },
    state: function () {return this._state;},
    unload: function () {
      for (var e = this, o = e._sounds, t = 0; t < o.length; t++) {
        if (o[t]._paused || e.stop(o[t]._id), !e._webAudio) {
          /MSIE |Trident\//.test(n._navigator && n._navigator.userAgent) ||
          (o[t]._node.src = 'data:audio/wav;base64,UklGRigAAABXQVZFZm10IBIAAAABAAEARKwAAIhYAQACABAAAABkYXRhAgAAAAEA'), o[t]._node.removeEventListener(
            'error', o[t]._errorFn, !1), o[t]._node.removeEventListener(
            n._canPlayEvent, o[t]._loadFn, !1);
        }
        delete o[t]._node, e._clearTimer(o[t]._id);
      }
      var a = n._howls.indexOf(e);
      a >= 0 && n._howls.splice(a, 1);
      var u = !0;
      for (t = 0; t < n._howls.length; t++) if (n._howls[t]._src === e._src) {
        u = !1;
        break;
      }
      return r && u &&
      delete r[e._src], n.noAudio = !1, e._state = 'unloaded', e._sounds = [], e = null, null;
    },
    on: function (e, n, o, t) {
      var r = this, a = r['_on' + e];
      return 'function' == typeof n &&
      a.push(t ? { id: o, fn: n, once: t } : { id: o, fn: n }), r;
    },
    off: function (e, n, o) {
      var t = this, r = t['_on' + e], a = 0;
      if ('number' == typeof n && (o = n, n = null), n || o) for (a = 0; a <
      r.length; a++) {
        var u = o === r[a].id;
        if (n === r[a].fn && u || !n && u) {
          r.splice(a, 1);
          break;
        }
      } else if (e) t['_on' + e] = []; else {
        var i = Object.keys(t);
        for (a = 0; a < i.length; a++) 0 === i[a].indexOf('_on') &&
        Array.isArray(t[i[a]]) && (t[i[a]] = []);
      }
      return t;
    },
    once: function (e, n, o) {
      var t = this;
      return t.on(e, n, o, 1), t;
    },
    _emit: function (e, n, o) {
      for (var t = this, r = t['_on' + e], a = r.length - 1; a >=
      0; a--) r[a].id && r[a].id !== n && 'load' !== e ||
      (setTimeout(function (e) {e.call(this, n, o);}.bind(t, r[a].fn),
        0), r[a].once && t.off(e, r[a].fn, r[a].id));
      return t._loadQueue(e), t;
    },
    _loadQueue: function (e) {
      var n = this;
      if (n._queue.length > 0) {
        var o = n._queue[0];
        o.event === e && (n._queue.shift(), n._loadQueue()), e || o.action();
      }
      return n;
    },
    _ended: function (e) {
      var o = this, t = e._sprite;
      if (!o._webAudio && e._node && !e._node.paused && !e._node.ended &&
        e._node.currentTime < e._stop) return setTimeout(o._ended.bind(o, e),
        100), o;
      var r = !(!e._loop && !o._sprite[t][2]);
      if (o._emit('end', e._id), !o._webAudio && r &&
      o.stop(e._id, !0).play(e._id), o._webAudio && r) {
        o._emit('play', e._id), e._seek = e._start ||
          0, e._rateSeek = 0, e._playStart = n.ctx.currentTime;
        var a = 1e3 * (e._stop - e._start) / Math.abs(e._rate);
        o._endTimers[e._id] = setTimeout(o._ended.bind(o, e), a);
      }
      return o._webAudio && !r &&
      (e._paused = !0, e._ended = !0, e._seek = e._start ||
        0, e._rateSeek = 0, o._clearTimer(e._id), o._cleanBuffer(
        e._node), n._autoSuspend()), o._webAudio || r || o.stop(e._id, !0), o;
    },
    _clearTimer: function (e) {
      var n = this;
      if (n._endTimers[e]) {
        if ('function' != typeof n._endTimers[e]) clearTimeout(
          n._endTimers[e]); else {
          var o = n._soundById(e);
          o && o._node &&
          o._node.removeEventListener('ended', n._endTimers[e], !1);
        }
        delete n._endTimers[e];
      }
      return n;
    },
    _soundById: function (e) {
      for (var n = this, o = 0; o < n._sounds.length; o++) if (e ===
        n._sounds[o]._id) return n._sounds[o];
      return null;
    },
    _inactiveSound: function () {
      var e = this;
      e._drain();
      for (var n = 0; n <
      e._sounds.length; n++) if (e._sounds[n]._ended) return e._sounds[n].reset();
      return new t(e);
    },
    _drain: function () {
      var e = this, n = e._pool, o = 0, t = 0;
      if (!(e._sounds.length < n)) {
        for (t = 0; t < e._sounds.length; t++) e._sounds[t]._ended && o++;
        for (t = e._sounds.length - 1; t >= 0; t--) {
          if (o <= n) return;
          e._sounds[t]._ended && (e._webAudio && e._sounds[t]._node &&
          e._sounds[t]._node.disconnect(0), e._sounds.splice(t, 1), o--);
        }
      }
    },
    _getSoundIds: function (e) {
      var n = this;
      if (void 0 === e) {
        for (var o = [], t = 0; t < n._sounds.length; t++) o.push(
          n._sounds[t]._id);
        return o;
      }
      return [e];
    },
    _refreshBuffer: function (e) {
      var o = this;
      return e._node.bufferSource = n.ctx.createBufferSource(), e._node.bufferSource.buffer = r[o._src], e._panner
        ? e._node.bufferSource.connect(e._panner)
        : e._node.bufferSource.connect(
          e._node), e._node.bufferSource.loop = e._loop, e._loop &&
      (e._node.bufferSource.loopStart = e._start ||
        0, e._node.bufferSource.loopEnd = e._stop ||
        0), e._node.bufferSource.playbackRate.setValueAtTime(e._rate,
        n.ctx.currentTime), o;
    },
    _cleanBuffer: function (e) {
      var o = this;
      if (n._scratchBuffer && e.bufferSource) {
        e.bufferSource.onended = null, e.bufferSource.disconnect(0);
        try {e.bufferSource.buffer = n._scratchBuffer;} catch (e) {}
      }
      return e.bufferSource = null, o;
    },
  };
  var t = function (e) {this._parent = e, this.init();};
  t.prototype = {
    init: function () {
      var e = this, o = e._parent;
      return e._muted = o._muted, e._loop = o._loop, e._volume = o._volume, e._rate = o._rate, e._seek = 0, e._paused = !0, e._ended = !0, e._sprite = '__default', e._id = ++n._counter, o._sounds.push(
        e), e.create(), e;
    },
    create: function () {
      var e = this, o = e._parent,
        t = n._muted || e._muted || e._parent._muted ? 0 : e._volume;
      return o._webAudio
        ? (e._node = void 0 === n.ctx.createGain
          ? n.ctx.createGainNode()
          : n.ctx.createGain(), e._node.gain.setValueAtTime(t,
          n.ctx.currentTime), e._node.paused = !0, e._node.connect(
          n.masterGain))
        : (e._node = new Audio, e._errorFn = e._errorListener.bind(
          e), e._node.addEventListener('error', e._errorFn,
          !1), e._loadFn = e._loadListener.bind(e), e._node.addEventListener(
          n._canPlayEvent, e._loadFn,
          !1), e._node.src = o._src, e._node.preload = 'auto', e._node.volume = t *
          n.volume(), e._node.load()), e;
    },
    reset: function () {
      var e = this, o = e._parent;
      return e._muted = o._muted, e._loop = o._loop, e._volume = o._volume, e._rate = o._rate, e._seek = 0, e._rateSeek = 0, e._paused = !0, e._ended = !0, e._sprite = '__default', e._id = ++n._counter, e;
    },
    _errorListener: function () {
      var e = this;
      e._parent._emit('loaderror', e._id,
        e._node.error ? e._node.error.code : 0), e._node.removeEventListener(
        'error', e._errorFn, !1);
    },
    _loadListener: function () {
      var e = this, o = e._parent;
      o._duration = Math.ceil(10 * e._node.duration) / 10, 0 ===
      Object.keys(o._sprite).length &&
      (o._sprite = { __default: [0, 1e3 * o._duration] }), 'loaded' !==
      o._state && (o._state = 'loaded', o._emit(
        'load'), o._loadQueue()), e._node.removeEventListener(n._canPlayEvent,
        e._loadFn, !1);
    },
  };
  var r = {}, a = function (e) {
      var n = e._src;
      if (r[n]) return e._duration = r[n].duration, void d(e);
      if (/^data:[^;]+;base64,/.test(n)) {
        for (var o = atob(n.split(',')[1]), t = new Uint8Array(
          o.length), a = 0; a < o.length; ++a) t[a] = o.charCodeAt(a);
        i(t.buffer, e);
      } else {
        var _ = new XMLHttpRequest;
        _.open('GET', n,
          !0), _.withCredentials = e._xhrWithCredentials, _.responseType = 'arraybuffer', _.onload = function () {
          var n = (_.status + '')[0];
          if ('0' !== n && '2' !== n && '3' !== n) return void e._emit(
            'loaderror', null,
            'Failed loading audio file with status: ' + _.status + '.');
          i(_.response, e);
        }, _.onerror = function () {
          e._webAudio &&
          (e._html5 = !0, e._webAudio = !1, e._sounds = [], delete r[n], e.load());
        }, u(_);
      }
    }, u = function (e) {try {e.send();} catch (n) {e.onerror();}},
    i = function (e, o) {
      var t = function () {
        o._emit('loaderror', null, 'Decoding audio data failed.');
      }, a = function (e) {
        e && o._sounds.length > 0
          ? (r[o._src] = e, d(o, e))
          : t();
      };
      'undefined' != typeof Promise && 1 === n.ctx.decodeAudioData.length
        ? n.ctx.decodeAudioData(e).then(a).catch(t)
        : n.ctx.decodeAudioData(e, a, t);
    }, d = function (e, n) {
      n && !e._duration && (e._duration = n.duration), 0 ===
      Object.keys(e._sprite).length &&
      (e._sprite = { __default: [0, 1e3 * e._duration] }), 'loaded' !==
      e._state && (e._state = 'loaded', e._emit('load'), e._loadQueue());
    }, _ = function () {
      try {
        'undefined' != typeof AudioContext
          ? n.ctx = new AudioContext
          : 'undefined' != typeof webkitAudioContext
          ? n.ctx = new webkitAudioContext
          : n.usingWebAudio = !1;
      } catch (e) {n.usingWebAudio = !1;}
      var e = /iP(hone|od|ad)/.test(n._navigator && n._navigator.platform),
        o = n._navigator &&
          n._navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/),
        t = o ? parseInt(o[1], 10) : null;
      if (e && t && t < 9) {
        var r = /safari/.test(
          n._navigator && n._navigator.userAgent.toLowerCase());
        (n._navigator && n._navigator.standalone && !r || n._navigator &&
          !n._navigator.standalone && !r) && (n.usingWebAudio = !1);
      }
      n.usingWebAudio && (n.masterGain = void 0 === n.ctx.createGain
        ? n.ctx.createGainNode()
        : n.ctx.createGain(), n.masterGain.gain.setValueAtTime(n._muted ? 0 : 1,
        n.ctx.currentTime), n.masterGain.connect(n.ctx.destination)), n._setup();
    };
  'function' == typeof define && define.amd &&
  define([], function () {return { Howler: n, Howl: o };}), 'undefined' !=
  typeof exports && (exports.Howler = n, exports.Howl = o), 'undefined' !=
  typeof window
    ? (window.HowlerGlobal = e, window.Howler = n, window.Howl = o, window.Sound = t)
    : 'undefined' != typeof global &&
    (global.HowlerGlobal = e, global.Howler = n, global.Howl = o, global.Sound = t);
}();
/*! Spatial Plugin */
!function () {
  'use strict';
  HowlerGlobal.prototype._pos = [
    0,
    0,
    0], HowlerGlobal.prototype._orientation = [
    0,
    0,
    -1,
    0,
    1,
    0], HowlerGlobal.prototype.stereo = function (e) {
    var n = this;
    if (!n.ctx || !n.ctx.listener) return n;
    for (var t = n._howls.length - 1; t >= 0; t--) n._howls[t].stereo(e);
    return n;
  }, HowlerGlobal.prototype.pos = function (e, n, t) {
    var r = this;
    return r.ctx && r.ctx.listener ? (n = 'number' != typeof n
      ? r._pos[1]
      : n, t = 'number' != typeof t ? r._pos[2] : t, 'number' != typeof e
      ? r._pos
      : (r._pos = [e, n, t], void 0 !== r.ctx.listener.positionX
        ? (r.ctx.listener.positionX.setTargetAtTime(r._pos[0],
          Howler.ctx.currentTime, .1), r.ctx.listener.positionY.setTargetAtTime(
          r._pos[1], Howler.ctx.currentTime,
          .1), r.ctx.listener.positionZ.setTargetAtTime(r._pos[2],
          Howler.ctx.currentTime, .1))
        : r.ctx.listener.setPosition(r._pos[0], r._pos[1], r._pos[2]), r)) : r;
  }, HowlerGlobal.prototype.orientation = function (
    e, n, t, r, o, i) {
    var a = this;
    if (!a.ctx || !a.ctx.listener) return a;
    var p = a._orientation;
    return n = 'number' != typeof n ? p[1] : n, t = 'number' != typeof t
      ? p[2]
      : t, r = 'number' != typeof r ? p[3] : r, o = 'number' != typeof o
      ? p[4]
      : o, i = 'number' != typeof i ? p[5] : i, 'number' != typeof e
      ? p
      : (a._orientation = [e, n, t, r, o, i], void 0 !== a.ctx.listener.forwardX
        ? (a.ctx.listener.forwardX.setTargetAtTime(e, Howler.ctx.currentTime,
          .1), a.ctx.listener.forwardY.setTargetAtTime(n,
          Howler.ctx.currentTime, .1), a.ctx.listener.forwardZ.setTargetAtTime(
          t, Howler.ctx.currentTime, .1), a.ctx.listener.upX.setTargetAtTime(e,
          Howler.ctx.currentTime, .1), a.ctx.listener.upY.setTargetAtTime(n,
          Howler.ctx.currentTime, .1), a.ctx.listener.upZ.setTargetAtTime(t,
          Howler.ctx.currentTime, .1))
        : a.ctx.listener.setOrientation(e, n, t, r, o, i), a);
  }, Howl.prototype.init = function (e) {
    return function (n) {
      var t = this;
      return t._orientation = n.orientation ||
        [1, 0, 0], t._stereo = n.stereo || null, t._pos = n.pos ||
        null, t._pannerAttr = {
        coneInnerAngle: void 0 !== n.coneInnerAngle ? n.coneInnerAngle : 360,
        coneOuterAngle: void 0 !== n.coneOuterAngle ? n.coneOuterAngle : 360,
        coneOuterGain: void 0 !== n.coneOuterGain ? n.coneOuterGain : 0,
        distanceModel: void 0 !== n.distanceModel ? n.distanceModel : 'inverse',
        maxDistance: void 0 !== n.maxDistance ? n.maxDistance : 1e4,
        panningModel: void 0 !== n.panningModel ? n.panningModel : 'HRTF',
        refDistance: void 0 !== n.refDistance ? n.refDistance : 1,
        rolloffFactor: void 0 !== n.rolloffFactor ? n.rolloffFactor : 1,
      }, t._onstereo = n.onstereo
        ? [{ fn: n.onstereo }]
        : [], t._onpos = n.onpos
        ? [{ fn: n.onpos }]
        : [], t._onorientation = n.onorientation
        ? [{ fn: n.onorientation }]
        : [], e.call(this, n);
    };
  }(Howl.prototype.init), Howl.prototype.stereo = function (n, t) {
    var r = this;
    if (!r._webAudio) return r;
    if ('loaded' !== r._state) return r._queue.push(
      { event: 'stereo', action: function () {r.stereo(n, t);} }), r;
    var o = void 0 === Howler.ctx.createStereoPanner ? 'spatial' : 'stereo';
    if (void 0 === t) {
      if ('number' != typeof n) return r._stereo;
      r._stereo = n, r._pos = [n, 0, 0];
    }
    for (var i = r._getSoundIds(t), a = 0; a < i.length; a++) {
      var p = r._soundById(i[a]);
      if (p) {
        if ('number' != typeof n) return p._stereo;
        p._stereo = n, p._pos = [n, 0, 0], p._node &&
        (p._pannerAttr.panningModel = 'equalpower', p._panner &&
        p._panner.pan || e(p, o), 'spatial' === o
          ? void 0 !== p._panner.positionX
            ? (p._panner.positionX.setValueAtTime(n,
              Howler.ctx.currentTime), p._panner.positionY.setValueAtTime(0,
              Howler.ctx.currentTime), p._panner.positionZ.setValueAtTime(0,
              Howler.ctx.currentTime))
            : p._panner.setPosition(n, 0, 0)
          : p._panner.pan.setValueAtTime(n, Howler.ctx.currentTime)), r._emit(
          'stereo', p._id);
      }
    }
    return r;
  }, Howl.prototype.pos = function (n, t, r, o) {
    var i = this;
    if (!i._webAudio) return i;
    if ('loaded' !== i._state) return i._queue.push(
      { event: 'pos', action: function () {i.pos(n, t, r, o);} }), i;
    if (t = 'number' != typeof t ? 0 : t, r = 'number' != typeof r
      ? -.5
      : r, void 0 === o) {
      if ('number' != typeof n) return i._pos;
      i._pos = [n, t, r];
    }
    for (var a = i._getSoundIds(o), p = 0; p < a.length; p++) {
      var s = i._soundById(a[p]);
      if (s) {
        if ('number' != typeof n) return s._pos;
        s._pos = [n, t, r], s._node &&
        (s._panner && !s._panner.pan || e(s, 'spatial'), void 0 !==
        s._panner.positionX ? (s._panner.positionX.setValueAtTime(n,
          Howler.ctx.currentTime), s._panner.positionY.setValueAtTime(t,
          Howler.ctx.currentTime), s._panner.positionZ.setValueAtTime(r,
          Howler.ctx.currentTime)) : s._panner.setOrientation(n, t,
          r)), i._emit('pos', s._id);
      }
    }
    return i;
  }, Howl.prototype.orientation = function (n, t, r, o) {
    var i = this;
    if (!i._webAudio) return i;
    if ('loaded' !== i._state) return i._queue.push({
      event: 'orientation',
      action: function () {i.orientation(n, t, r, o);},
    }), i;
    if (t = 'number' != typeof t ? i._orientation[1] : t, r = 'number' !=
    typeof r ? i._orientation[2] : r, void 0 === o) {
      if ('number' != typeof n) return i._orientation;
      i._orientation = [n, t, r];
    }
    for (var a = i._getSoundIds(o), p = 0; p < a.length; p++) {
      var s = i._soundById(a[p]);
      if (s) {
        if ('number' != typeof n) return s._orientation;
        s._orientation = [n, t, r], s._node && (s._panner ||
        (s._pos || (s._pos = i._pos || [0, 0, -.5]), e(s,
          'spatial')), void 0 !== s._panner.orientationX
          ? (s._panner.orientationX.setValueAtTime(n,
            Howler.ctx.currentTime), s._panner.orientationY.setValueAtTime(t,
            Howler.ctx.currentTime), s._panner.orientationZ.setValueAtTime(r,
            Howler.ctx.currentTime))
          : s._panner.setOrientation(n, t, r)), i._emit('orientation', s._id);
      }
    }
    return i;
  }, Howl.prototype.pannerAttr = function () {
    var n, t, r, o = this, i = arguments;
    if (!o._webAudio) return o;
    if (0 === i.length) return o._pannerAttr;
    if (1 === i.length) {
      if ('object' != typeof i[0]) return r = o._soundById(
        parseInt(i[0], 10)), r ? r._pannerAttr : o._pannerAttr;
      n = i[0], void 0 === t && (n.pannerAttr || (n.pannerAttr = {
        coneInnerAngle: n.coneInnerAngle,
        coneOuterAngle: n.coneOuterAngle,
        coneOuterGain: n.coneOuterGain,
        distanceModel: n.distanceModel,
        maxDistance: n.maxDistance,
        refDistance: n.refDistance,
        rolloffFactor: n.rolloffFactor,
        panningModel: n.panningModel,
      }), o._pannerAttr = {
        coneInnerAngle: void 0 !== n.pannerAttr.coneInnerAngle
          ? n.pannerAttr.coneInnerAngle
          : o._coneInnerAngle,
        coneOuterAngle: void 0 !== n.pannerAttr.coneOuterAngle
          ? n.pannerAttr.coneOuterAngle
          : o._coneOuterAngle,
        coneOuterGain: void 0 !== n.pannerAttr.coneOuterGain
          ? n.pannerAttr.coneOuterGain
          : o._coneOuterGain,
        distanceModel: void 0 !== n.pannerAttr.distanceModel
          ? n.pannerAttr.distanceModel
          : o._distanceModel,
        maxDistance: void 0 !== n.pannerAttr.maxDistance
          ? n.pannerAttr.maxDistance
          : o._maxDistance,
        refDistance: void 0 !== n.pannerAttr.refDistance
          ? n.pannerAttr.refDistance
          : o._refDistance,
        rolloffFactor: void 0 !== n.pannerAttr.rolloffFactor
          ? n.pannerAttr.rolloffFactor
          : o._rolloffFactor,
        panningModel: void 0 !== n.pannerAttr.panningModel
          ? n.pannerAttr.panningModel
          : o._panningModel,
      });
    } else 2 === i.length && (n = i[0], t = parseInt(i[1], 10));
    for (var a = o._getSoundIds(t), p = 0; p <
    a.length; p++) if (r = o._soundById(a[p])) {
      var s = r._pannerAttr;
      s = {
        coneInnerAngle: void 0 !== n.coneInnerAngle
          ? n.coneInnerAngle
          : s.coneInnerAngle,
        coneOuterAngle: void 0 !== n.coneOuterAngle
          ? n.coneOuterAngle
          : s.coneOuterAngle,
        coneOuterGain: void 0 !== n.coneOuterGain
          ? n.coneOuterGain
          : s.coneOuterGain,
        distanceModel: void 0 !== n.distanceModel
          ? n.distanceModel
          : s.distanceModel,
        maxDistance: void 0 !== n.maxDistance ? n.maxDistance : s.maxDistance,
        refDistance: void 0 !== n.refDistance ? n.refDistance : s.refDistance,
        rolloffFactor: void 0 !== n.rolloffFactor
          ? n.rolloffFactor
          : s.rolloffFactor,
        panningModel: void 0 !== n.panningModel
          ? n.panningModel
          : s.panningModel,
      };
      var c = r._panner;
      c
        ? (c.coneInnerAngle = s.coneInnerAngle, c.coneOuterAngle = s.coneOuterAngle, c.coneOuterGain = s.coneOuterGain, c.distanceModel = s.distanceModel, c.maxDistance = s.maxDistance, c.refDistance = s.refDistance, c.rolloffFactor = s.rolloffFactor, c.panningModel = s.panningModel)
        : (r._pos || (r._pos = o._pos || [0, 0, -.5]), e(r, 'spatial'));
    }
    return o;
  }, Sound.prototype.init = function (e) {
    return function () {
      var n = this, t = n._parent;
      n._orientation = t._orientation, n._stereo = t._stereo, n._pos = t._pos, n._pannerAttr = t._pannerAttr, e.call(
        this), n._stereo ? t.stereo(n._stereo) : n._pos &&
        t.pos(n._pos[0], n._pos[1], n._pos[2], n._id);
    };
  }(Sound.prototype.init), Sound.prototype.reset = function (e) {
    return function () {
      var n = this, t = n._parent;
      return n._orientation = t._orientation, n._stereo = t._stereo, n._pos = t._pos, n._pannerAttr = t._pannerAttr, n._stereo
        ? t.stereo(n._stereo)
        : n._pos ? t.pos(n._pos[0], n._pos[1], n._pos[2], n._id) : n._panner &&
          (n._panner.disconnect(0), n._panner = void 0, t._refreshBuffer(
            n)), e.call(this);
    };
  }(Sound.prototype.reset);
  var e = function (e, n) {
    n = n || 'spatial', 'spatial' === n
      ? (e._panner = Howler.ctx.createPanner(), e._panner.coneInnerAngle = e._pannerAttr.coneInnerAngle, e._panner.coneOuterAngle = e._pannerAttr.coneOuterAngle, e._panner.coneOuterGain = e._pannerAttr.coneOuterGain, e._panner.distanceModel = e._pannerAttr.distanceModel, e._panner.maxDistance = e._pannerAttr.maxDistance, e._panner.refDistance = e._pannerAttr.refDistance, e._panner.rolloffFactor = e._pannerAttr.rolloffFactor, e._panner.panningModel = e._pannerAttr.panningModel, void 0 !==
      e._panner.positionX ? (e._panner.positionX.setValueAtTime(e._pos[0],
        Howler.ctx.currentTime), e._panner.positionY.setValueAtTime(e._pos[1],
        Howler.ctx.currentTime), e._panner.positionZ.setValueAtTime(e._pos[2],
        Howler.ctx.currentTime)) : e._panner.setPosition(e._pos[0], e._pos[1],
        e._pos[2]), void 0 !== e._panner.orientationX
        ? (e._panner.orientationX.setValueAtTime(e._orientation[0],
          Howler.ctx.currentTime), e._panner.orientationY.setValueAtTime(
          e._orientation[1],
          Howler.ctx.currentTime), e._panner.orientationZ.setValueAtTime(
          e._orientation[2], Howler.ctx.currentTime))
        : e._panner.setOrientation(e._orientation[0], e._orientation[1],
          e._orientation[2]))
      : (e._panner = Howler.ctx.createStereoPanner(), e._panner.pan.setValueAtTime(
        e._stereo, Howler.ctx.currentTime)), e._panner.connect(
      e._node), e._paused || e._parent.pause(e._id, !0).play(e._id, !0);
  };
}();

countdown.setLabels(
  ' 毫秒| 秒| 分| 时| 天| 周| 月| 年| 十| 世纪| 千年',
  ' 毫秒| 秒| 分| 时| 天| 周| 月| 年| 十| 世纪| 千年',
  ' &nbsp; ',
  ' &nbsp; ',
  '就是现在');

function isTerminated () {
  currT = new Date(new Date().toUTCString());
  if (END_T - currT < 1000) {
    return true;
  } else {
    return false;
  }
}

var pageTimer = document.getElementById('pageTimer');

var timerId =
  countdown(
    END_T,
    function (ts) {
      pageTimer.innerHTML = ts.toHTML('strong');
      if (isTerminated()) {
        window.clearInterval(timerId);
        location.reload();
      }
    },
    countdown.DAYS | countdown.HOURS | countdown.MINUTES | countdown.SECONDS);

const notes = {
  1: new Howl({ src: ['/static/notes/523-C.mp3'] }),
  2: new Howl({ src: ['/static/notes/587-D.mp3'] }),
  3: new Howl({ src: ['/static/notes/659-E.mp3'] }),
  4: new Howl({ src: ['/static/notes/698-F.mp3'] }),
  5: new Howl({ src: ['/static/notes/783-G.mp3'] }),
};

const transcripts = [
  [
    3, 3, 3, 3, 3, 3, 3, 5, 1, 2, 3,
    4, 4, 4, 4, 3, 3, 3, 2, 2, 3, 2, 5,
    3, 3, 3, 3, 3, 3, 3, 5, 1, 2, 3,
    4, 4, 4, 4, 3, 3, 5, 5, 4, 2, 1,
    3, 5, 3, 1,
  ].reverse(),
];

function handlePlay (e) {
  if (transcripts[0].length > 0) {
    var note = notes[transcripts[0].pop()];
    note.play();
  }
}

window.onload = function startup () {
  document.addEventListener('click', handlePlay, false);
};

(function () {
  var requestAnimationFrame = window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame || window.msRequestAnimationFrame ||
    function (callback) {
      window.setTimeout(callback, 1000 / 60);
    };
  window.requestAnimationFrame = requestAnimationFrame;
})();

var flakes = [],
  canvas = document.getElementById('canvas'),
  ctx = canvas.getContext('2d'),
  flakeCount = Math.ceil(
    50 * window.innerWidth / 375 * window.innerHeight / 635),
  mX = -100,
  mY = -100;

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

function snow () {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  for (var i = 0; i < flakeCount; i++) {
    var flake = flakes[i],
      x = mX,
      y = mY,
      minDist = 80,
      x2 = flake.x,
      y2 = flake.y;

    var dist = Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y)),
      dx = x2 - x,
      dy = y2 - y;

    if (dist < minDist) {
      var force = minDist / (dist * dist),
        xcomp = (x - x2) / dist,
        ycomp = (y - y2) / dist,
        deltaV = force / 2;

      flake.velX -= deltaV * xcomp;
      flake.velY -= deltaV * ycomp;

    } else {
      flake.velX *= .98;
      if (flake.velY <= flake.speed) {
        flake.velY = flake.speed;
      }
      flake.velX += Math.cos(flake.step += .05) * flake.stepSize;
    }

    ctx.fillStyle = 'rgba(255,255,255,' + flake.opacity + ')';
    flake.y += flake.velY;
    flake.x += flake.velX;

    if (flake.y >= canvas.height || flake.y <= 0) {
      reset(flake);
    }

    if (flake.x >= canvas.width || flake.x <= 0) {
      reset(flake);
    }

    ctx.beginPath();
    ctx.arc(flake.x, flake.y, flake.size, 0, Math.PI * 2);
    ctx.fill();
  }
  requestAnimationFrame(snow);
};

function reset (flake) {
  flake.x = Math.floor(Math.random() * canvas.width);
  flake.y = 0;
  flake.size = (Math.random() * 3) + 2;
  flake.speed = (Math.random() * 1) + 0.5;
  flake.velY = flake.speed;
  flake.velX = 0;
  flake.opacity = (Math.random() * 0.5) + 0.3;
}

function init () {
  for (var i = 0; i < flakeCount; i++) {
    var x = Math.floor(Math.random() * canvas.width),
      y = Math.floor(Math.random() * canvas.height),
      size = (Math.random() * 3) + 2,
      speed = (Math.random() * 1) + 0.5,
      opacity = (Math.random() * 0.5) + 0.3;

    flakes.push({
      speed: speed,
      velY: speed,
      velX: 0,
      x: x,
      y: y,
      size: size,
      stepSize: (Math.random()) / 30,
      step: 0,
      opacity: opacity,
    });
  }

  snow();
};

canvas.addEventListener('mousemove', function (e) {
  mX = e.clientX,
    mY = e.clientY;
});

window.addEventListener('resize', function () {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
});

init();