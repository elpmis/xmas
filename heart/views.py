from django.views.generic import TemplateView


class HeartView(TemplateView):
    template_name = "heart.html"
