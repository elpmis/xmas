from django.views.generic import TemplateView
import datetime


class CountdownView(TemplateView):
    template_name = "countdown.html"
    pwd_template_name = "pwd.html"
    year = 2018
    month = 12
    day = 25
    hour = 0
    minute = 0

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['year'] = self.year
        context['month'] = self.month
        context['day'] = self.day
        context['hour'] = self.hour
        context['minute'] = self.minute
        return context

    def get_template_names(self):
        if not self.is_christmas():
            return super().get_template_names()
        else:
            return self.pwd_template_name

    def is_christmas(self):
        if datetime.datetime.utcnow() < datetime.datetime(self.year, self.month, self.day, self.hour, self.minute):
            return False
        else:
            return True
